/*
 * Copyright 2017, Korvin F. Ezüst
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.korvin.trequiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    int score    = 0;
    int maxScore = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int rAnswers[] = {
                R.id.Q1A1,
                R.id.Q1A2,
                R.id.Q1A3,
                R.id.Q1C,
                R.id.Q2A1,
                R.id.Q2A2,
                R.id.Q2A3,
                R.id.Q2C,
                R.id.Q3A1,
                R.id.Q3A2,
                R.id.Q3A3,
                R.id.Q3C,
                R.id.Q4A1,
                R.id.Q4A2,
                R.id.Q4A3,
                R.id.Q4C,
                R.id.Q5A1,
                R.id.Q5A2,
                R.id.Q5A3,
                R.id.Q5C};

        for (int item : rAnswers)
            radioButtonListener(item);

        int mAnswers[] = {
                R.id.Q6A1,
                R.id.Q6A2,
                R.id.Q6C1,
                R.id.Q6C2,
                R.id.Q7A1,
                R.id.Q7A2,
                R.id.Q7A3,
                R.id.Q7C,
                R.id.Q8A1,
                R.id.Q8A2,
                R.id.Q8C1,
                R.id.Q8C2,
                R.id.Q9A1,
                R.id.Q9A2,
                R.id.Q9A3,
                R.id.Q9C,};

        for (int item : mAnswers)
            checkBoxListener(item);
    }

    /**
     * Changes a CheckBox's background and text color when selected or unselected.
     *
     * @param id a CheckBox ID
     */
    private void checkBoxListener(int id)
    {
        CheckBox checkbox = findViewById(id);
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                if (b)
                {
                    compoundButton.setBackgroundColor(getResources().getColor(R.color.kindaYellow));
                    compoundButton.setTextColor(getResources().getColor(R.color.black));
                }
                else
                {
                    compoundButton.setBackgroundColor(getResources().getColor(R.color.black));
                    compoundButton.setTextColor(getResources().getColor(R.color.kindaYellow));
                }
            }
        });
    }

    /**
     * Changes the selected RadioButton's background and text color.
     *
     * @param id a RadioButton ID
     */
    private void radioButtonListener(int id)
    {
        RadioButton radiobutton = findViewById(id);
        radiobutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                if (b)
                {
                    compoundButton.setBackgroundColor(getResources().getColor(R.color.kindaYellow));
                    compoundButton.setTextColor(getResources().getColor(R.color.black));
                }
                else
                {
                    compoundButton.setBackgroundColor(getResources().getColor(R.color.black));
                    compoundButton.setTextColor(getResources().getColor(R.color.kindaYellow));
                }
            }
        });
    }

    /**
     * Hides the welcome screen and shows the first question.
     */
    public void startQuiz(View v)
    {
        hideView(R.id.layoutTitle);
        showView(R.id.layoutQ1);
        showView(R.id.Question);
    }

    /**
     * Checks the answer to the first question
     * and replaces it with the second question.
     */
    public void next1Clicked(View v)
    {
        nextClickedRadio(R.id.Q1C, R.string.Q2);
        clearCheck(R.id.Q1C);
        hideView(R.id.layoutQ1);
        showView(R.id.layoutQ2);
    }

    /**
     * Checks the answer to the second question
     * and replaces it with the third question.
     */
    public void next2Clicked(View v)
    {
        nextClickedRadio(R.id.Q2C, R.string.Q3);
        clearCheck(R.id.Q2C);
        hideView(R.id.layoutQ2);
        showView(R.id.layoutQ3);
    }

    /**
     * Checks the answer to the third question
     * and replaces it with the fourth question.
     */
    public void next3Clicked(View v)
    {
        nextClickedRadio(R.id.Q3C, R.string.Q4);
        clearCheck(R.id.Q3C);
        hideView(R.id.layoutQ3);
        showView(R.id.layoutQ4);
    }

    /**
     * Checks the answer to the fourth question
     * and replaces it with the fifth question.
     */
    public void next4Clicked(View v)
    {
        nextClickedRadio(R.id.Q4C, R.string.Q5);
        clearCheck(R.id.Q4C);
        hideView(R.id.layoutQ4);
        showView(R.id.layoutQ5);
    }

    /**
     * Checks the answer to the fifth question
     * and replaces it with the sixth question.
     */
    public void next5Clicked(View v)
    {
        nextClickedRadio(R.id.Q5C, R.string.Q6);
        clearCheck(R.id.Q5C);
        hideView(R.id.layoutQ5);
        showView(R.id.layoutQ6);
    }

    /**
     * Checks the answer to the sixth question
     * and replaces it with the seventh question.
     */
    public void next6Clicked(View v)
    {
        nextClickMulti(R.id.Q6A1,
                       R.id.Q6A2,
                       R.id.Q6C1,
                       R.id.Q6C2,
                       false,
                       false,
                       true,
                       true,
                       R.string.Q7);
        clearCheck(R.id.Q6C1);
        clearCheck(R.id.Q6C2);
        hideView(R.id.layoutQ6);
        showView(R.id.layoutQ7);
    }

    /**
     * Checks the answer to the seventh question
     * and replaces it with the eight question.
     */
    public void next7Clicked(View v)
    {
        nextClickMulti(R.id.Q7A1,
                       R.id.Q7A2,
                       R.id.Q7A3,
                       R.id.Q7C,
                       false,
                       false,
                       false,
                       true,
                       R.string.Q8);
        clearCheck(R.id.Q7C);
        hideView(R.id.layoutQ7);
        showView(R.id.layoutQ8);
    }

    /**
     * Checks the answer to the eight question
     * and replaces it with the ninth question.
     */
    public void next8Clicked(View v)
    {
        nextClickMulti(R.id.Q8A1,
                       R.id.Q8A2,
                       R.id.Q8C1,
                       R.id.Q8C2,
                       false,
                       false,
                       true,
                       true,
                       R.string.Q9);
        clearCheck(R.id.Q8C1);
        clearCheck(R.id.Q8C2);
        hideView(R.id.layoutQ8);
        showView(R.id.layoutQ9);
    }

    /**
     * Checks the answer to the ninth question
     * and replaces it with the tenth question.
     */
    public void next9Clicked(View v)
    {
        nextClickMulti(R.id.Q9A1,
                       R.id.Q9A2,
                       R.id.Q9A3,
                       R.id.Q9C,
                       false,
                       false,
                       false,
                       true,
                       R.string.Q10);
        clearCheck(R.id.Q9C);
        hideView(R.id.layoutQ9);
        showView(R.id.layoutQ10);

        EditText editText = findViewById(R.id.answerInput);
        editText.requestFocus();
    }

    /**
     * Checks the answer to the tenth question
     * and replaces it with the summary.
     * Replaces the question's text with the score.
     * Displays a toast with the score.
     */
    public void evaluate(View v)
    {
        hideView(R.id.Question);

        EditText ans    = findViewById(R.id.answerInput);
        String   answer = ans.getText().toString().toLowerCase();

        CharSequence there     = "there";
        CharSequence are       = "are";
        CharSequence four      = "four";
        CharSequence fourDigit = "4";
        CharSequence lights    = "lights";

        if (answer.contains(there) && answer.contains(are) && (
                answer.contains(four) || answer.contains(fourDigit)) && answer.contains(lights))
            score += 1;

        hideView(R.id.layoutQ10);
        showView(R.id.summary);

        TextView textView = findViewById(R.id.score);
        String   s        = getString(R.string.score, score, maxScore);
        textView.setText(s);

        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }

    /**
     * Sets a currently hidden (gone) View to be visible
     *
     * @param id a View ID
     */
    private void showView(int id)
    {
        View view = findViewById(id);
        view.setVisibility(View.VISIBLE);
    }

    /**
     * Sets a currently visible View to be hidden (gone)
     *
     * @param id a View ID
     */
    private void hideView(int id)
    {
        View view = findViewById(id);
        view.setVisibility(View.GONE);
    }

    /**
     * Unchecks a RadioButton or CheckBox
     *
     * @param id a CompoundButton ID
     */
    private void clearCheck(int id)
    {
        CompoundButton check = findViewById(id);
        check.setChecked(false);
    }

    /**
     * Checks if the given CheckBox or RadioButton is checked
     *
     * @param id a CompoundButton ID
     *
     * @return true or false
     */
    private boolean quickCheck(int id)
    {
        CompoundButton button = findViewById(id);
        return button.isChecked();
    }

    /**
     * Changes a TextView's text, used to replace the question
     * with the next one each time the user presses the "Next Question" button.
     *
     * @param id a TextView ID, the next question
     */
    private void nextView(int id)
    {
        TextView question = findViewById(R.id.Question);
        question.setText(getText(id));
    }

    /**
     * Checks if a RadioButton is checked, adds 1 to the score if it is
     * and replaces the question with the next one.
     *
     * @param id a RadioButton ID, the user's answer
     * @param s  a TextView ID, the next question
     */
    private void nextClickedRadio(int id, int s)
    {
        if (quickCheck(id))
            score += 1;

        nextView(s);
    }

    /**
     * Checks if the user selected the correct answers
     * from the given four.
     * Adds 1 to the score if the correct ones and only
     * the correct ones are checked.
     *
     * @param id1 CompoundButton ID, first answer
     * @param id2 CompoundButton ID, second answer
     * @param id3 CompoundButton ID, third answer
     * @param id4 CompoundButton ID, fourth answer
     * @param b1  boolean, must be true if the first answer is correct false otherwise
     * @param b2  boolean, must be true if the second answer is correct false otherwise
     * @param b3  boolean, must be true if the third answer is correct false otherwise
     * @param b4  boolean, must be true if the fourth answer is correct false otherwise
     * @param s   TextView ID, the next question
     */
    private void nextClickMulti(
            int id1,
            int id2,
            int id3,
            int id4,
            boolean b1,
            boolean b2,
            boolean b3,
            boolean b4,
            int s)
    {
        if (quickCheck(id1) == b1 && quickCheck(id2) == b2 && quickCheck(id3) == b3 && quickCheck(
                id4) == b4)
            score += 1;

        nextView(s);
    }

    /**
     * Quits the app.
     */
    public void exitOnClick(View v)
    {
        finish();
        System.exit(0);
    }
}
