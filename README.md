# TreQuiz
This project was made for the Quiz App project of the _"Google Developer Challenge Scholarship: Android Basics"_ course on Udacity.  

It asks 10 questions related to Star Trek, then displays the score and the correct answers with some additional information.

## Screenshots

<img src="screenshots/screenshot_1.png" width="200"> <img src="screenshots/screenshot_2.png" width="200"> <img src="screenshots/screenshot_3.png" width="200"> <img src="screenshots/screenshot_4.png" width="200">

